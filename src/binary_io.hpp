/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "common.hpp"

#include <cstdint>
#include <type_traits>
#include <algorithm>
#include <string>
#include <fstream>
#include <map>
#include <Magick++.h>


template<typename T>
struct IsOFstream : std::false_type {};

template<>
struct IsOFstream<std::ofstream> : std::true_type {};

template<>
struct IsOFstream<std::fstream> : std::true_type {};

template<typename T>
struct IsIFstream : std::false_type {};

template<>
struct IsIFstream<std::ifstream> : std::true_type {};

template<>
struct IsIFstream<std::fstream> : std::true_type {};


class Endianess {
	private:
	Endianess() = delete;
	//41 42 43 44 = 'ABCD' hex ASCII code
	static constexpr std::uint32_t LITTLE_{0x41424344u};

	//44 43 42 41 = 'DCBA' hex ASCII code
	static constexpr std::uint32_t BIG_{0x44434241u};

	//Converts chars to uint32 on current platform
	static constexpr std::uint32_t NATIVE_{'ABCD'};
	public:
	enum class Endian {
		LITTLE,
		BIG,
		UNKNOWN
	};

	constexpr static const Endian NATIVE_ENDIAN = NATIVE_ == LITTLE_ ? Endian::LITTLE : Endian::BIG;
	constexpr static const Endian INVERSE_NATIVE_ENDIAN = NATIVE_ == LITTLE_ ? Endian::BIG : Endian::LITTLE;

	constexpr static const Endian LITTLE_ENDIAN = Endian::LITTLE;
	constexpr static const Endian BIG_ENDIAN = Endian::BIG;

	constexpr static const Endian UNKOWN_ENDIAN = Endian::UNKNOWN;
};

template<typename S>
class binary_io {
	private:
	Endianess::Endian endian;

	static inline void read_fail() {
		throw ZToolsError("failed to read data from stream. It might be corrupt.");
	}

	static inline void write_fail() {
		throw ZToolsError("failed to write data to stream.");
	}

	template<typename T>
	static inline void staticAssertArithmetic() {
		//static_assert(std::is_arithmetic<T>::value);
	}


	template<typename T, typename dummy = void>
	inline auto raw_read_value(T &n) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		if (!stream.read(reinterpret_cast<char*>(&n), sizeof(n))) {
			read_fail();
		}
	}

	template<typename T, typename dummy = void>
	inline auto raw_write_value(const T n) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		if (!stream.write(reinterpret_cast<const char*>(&n), sizeof(n))) {
			write_fail();
		}
	}

	template<typename T, typename dummy = void>
	inline auto read_ordered_value(T &n) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		if (endian == Endianess::UNKOWN_ENDIAN) {
			throw Error("Undefined endianness.");
		}

		raw_read_value(n);

		if (endian == Endianess::INVERSE_NATIVE_ENDIAN) {
			reorder(n);
		}
	}

	template<typename T, typename dummy = void>
	inline auto write_ordered_value(T n) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		if (endian == Endianess::UNKOWN_ENDIAN) {
			throw Error("Undefined endianness.");
		}

		if (endian == Endianess::INVERSE_NATIVE_ENDIAN) {
			reorder(n);
		}

		raw_write_value(n);
	}


	public:
	S stream;

	binary_io() : endian(Endianess::UNKOWN_ENDIAN) {}
	binary_io(Endianess::Endian _endian) : endian(_endian) {}

	template<typename T>
	static void reorder(T &n) {
		char *nptr = reinterpret_cast<char*>(&n);
		std::reverse(nptr, nptr + sizeof(n));
	}


	void setEndian(Endianess::Endian newEndian) {
		endian = newEndian;
	}

	Endianess::Endian getEndian() {
		return endian;
	}

	template<typename T, typename dummy = void>
	inline auto raw_read_numeric(T &n) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		staticAssertArithmetic<T>();
		raw_read_value(n);
	}

	template<typename T, typename dummy = void>
	inline auto raw_write_numeric(const T n) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		staticAssertArithmetic<T>();
		raw_write_value(n);
	}

	template<typename T, typename dummy = void>
	inline auto read_numeric(T &n) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		staticAssertArithmetic<T>();
		read_ordered_value(n);
	}

	template<typename T, typename dummy = void>
	inline auto write_numeric(T &n) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		staticAssertArithmetic<T>();
		write_ordered_value(n);
	}

	template<typename dummy = void>
	inline auto read_null_term_string(std::string &str) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		std::getline(stream, str, '\0')
	}

	template<typename dummy = void>
	inline auto read_len_string(std::string &str, const size_t len) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		char buffer[1 << 15];
		if (len > sizeof(buffer)) {
			throw ZToolsError(strformat("Attempt to read a string larger than %lu bytes. The stream might be corrupt.", (unsigned long)sizeof(buffer)));
		}

		if (!stream.read(buffer, len)) {
			read_fail();
		}

		str = buffer;
		str.resize(len);
	}

	template<typename dummy = void>
	inline auto read_kstring(std::string &str) -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		size_t len;
		read_numeric(len);
		read_len_string(str, len);
	}

	template<typename dummy = void>
	inline auto write_null_term_string(std::string &str) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		if (!stream.write(str.data(), str.length()) and !stream.write("\x00", 1)) {
			write_fail();
		}
	}

	template<typename dummy = void>
	inline auto write_len_string(std::string &str, const size_t len) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		if (!stream.write(str.data(), len)) {
			write_fail();
		}
	}

	template<typename dummy = void>
	inline auto write_kstring(std::string &str) -> std::enable_if_t<IsOFstream<S>::value, dummy> {
		const size_t len = str.length();
		write_numeric(len);
		write_len_string(str, len);
	}

	template<typename dummy = void>
	inline auto skip_kstring() -> std::enable_if_t<IsIFstream<S>::value, dummy> {
		size_t len;
		read_numeric(len);
		stream.ignore(len);
	}
};

static void get_magic_number(const fs::path & path, std::string & out) {
	binary_io<std::ifstream> file{Endianess::LITTLE_ENDIAN};
	file.stream.open(path, std::ios::in | std::ios::binary);
	if (file.stream.is_open()) {
		file.read_len_string(out, 4);
		file.stream.close();
	}
}

static std::string get_file_type(const fs::path& path) {
	if (path.extension() == ".xml") {
		return "XML";
	} else if(path.extension() == ".scml") {
		return "SCML";
	}
	std::string magic;
	get_magic_number(path, magic);
	if (magic == "KTEX" || magic == "BILD" || magic == "ANIM") {
		return magic;
	}
	try {
		Magick::Image is_image;
		is_image.ping(path.string());
		return "IMG";
	} catch(Magick::Exception e) {
		return "UNK";
	}
}

static std::map<std::string, uint32_t> hashcollection;
static uint32_t strhash(std::string str) {
	if (hashcollection.count(str) == 1) {
		return hashcollection[str];
	}
	uint32_t hash = 0;
	for (auto & c : str) {
		hash = (tolower(c) + (hash << 6) + (hash << 16) - hash);// &0xFFFFFFFFLL;
	}
	hashcollection[str] = hash;
	return hash;
}
static void fillLayers() {
	for (int i = 0; i < 1025; i++) {
		strhash(strformat("Layer %i", i));
	}
}