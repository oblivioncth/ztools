/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "config.h"
#include <cstdarg>
#include <cstdio>
#include <string>
#include <filesystem>

namespace fs = std::filesystem;

#ifdef _MSC_VER
// Mostly to avoid warnings related to ImageMagick classes.
#	pragma warning( disable: 4251 )
#	pragma warning( disable: 4275 )
// warning for unrefereneced function
#	pragma warning( disable: 4505 )
#endif

#if defined(_WIN32) || defined(WIN32)
#	define IS_WINDOWS 1
#elif defined(__linux__)
#	define IS_LINUX 1
#elif defined(__APPLE__) && defined(__MACH__)
#	define IS_MAC 1
#endif

#if !defined(IS_WINDOWS)
#	if defined(IS_LINUX) || defined(IS_MAC) || defined(BSD) || defined(unix) || defined(__unix__) || defined(__unix)
#		define IS_UNIX
#	endif
#endif

#if !defined(IS_UNIX) && !defined(IS_WINDOWS)
#	error "Unknown operating system"
#endif

#ifdef __GNUC__
#	define PRINTFSTYLE(fmt_index, first_to_check) __attribute__ ((format (printf, fmt_index, first_to_check)))
#	define DEPRECATEDFUNCTION __attribute__ ((deprecated))
#	define CONSTFUNCTION __attribute__ ((const))
#	define PUREFUNCTION __attribute__ ((pure))
#	if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 3)
#		define HOTFUNCTION __attribute__ ((hot))
#	endif
#endif

#ifndef PRINTFSTYLE
#	define PRINTFSTYLE(fmt_index, first_to_check)
#endif
#ifndef DEPRECATEDFUNCTION
#	define DEPRECATEDFUNCTION
#endif
#ifndef CONSTFUNCTION
#	define CONSTFUNCTION
#endif
#ifndef PUREFUNCTION
#	define PUREFUNCTION
#endif
#ifndef HOTFUNCTION
#	define HOTFUNCTION
#endif

#ifdef IS_WINDOWS
#	ifndef _USE_MATH_DEFINES
#		define _USE_MATH_DEFINES 1
#	endif
#	ifndef NOMINMAX
#		define NOMINMAX 1
#	endif
#endif


#ifndef HAVE_SNPRINTF
#	ifdef HAVE__SNPRINTF
#		define snprintf _snprintf
#		define HAVE_SNPRINTF 1
#	else
int snprintf(char *str, size_t n, const char *fmt, ...) PRINTFSTYLE(3, 4);
#	endif
#endif

#ifndef HAVE_SNPRINTF
int vsnprintf(char *str, size_t n, const char *fmt, va_list ap) {
	(void) n;
	return vsprintf(str, fmt, ap);
}

int snprintf(char *str, size_t n, const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);

	const int ret = vsnprintf(str, n, fmt, ap);

	va_end(ap);

	return ret;
}
#endif

static int vstrformat_inplace(std::string& s, const char *fmt, va_list ap) {
	char buffer[1 << 15];

	const int ret = vsnprintf(buffer, sizeof(buffer) - 1, fmt, ap);

	if (ret > 0) {
		s.assign(buffer, ret);
	} else {
		s.clear();
	}

	return ret;
}

static int strformat_inplace(std::string& s, const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);

	const int ret = vstrformat_inplace(s, fmt, ap);

	va_end(ap);

	return ret;
}

static std::string strformat(const char *fmt, ...) {
	std::string s;

	va_list ap;
	va_start(ap, fmt);

	(void) vstrformat_inplace(s, fmt, ap);

	va_end(ap);

	return s;
}

static uint16_t upper_power_of_two(uint16_t v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v++;
	return v;
}

static uint32_t upper_power_of_two(uint32_t v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return v;
}

class Error : public std::exception {
	public:
	Error(const std::string& cppwhat_) : cppwhat(cppwhat_) {}
	virtual ~Error() throw() {}

	virtual char const* what() const throw() { return cppwhat.c_str(); }

	protected:
	void setMessage(const std::string& cppwhat_) {
		cppwhat = cppwhat_;
	}

	Error() : cppwhat() {}

	private:
	std::string cppwhat;
};

class ZToolsError : public Error {
	public:
	ZToolsError(const std::string& what) : Error(what) {}
	virtual ~ZToolsError() throw() {}
};