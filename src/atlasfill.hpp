/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#include "common.hpp"
#include <Magick++.h>

class parsed_command_line;

namespace AtlasFill {

	struct atlas_data {
		uint16_t width;
		uint16_t height;
		struct atlas_image_data {
			fs::path path;
			uint16_t width;
			uint16_t height;
			uint16_t xoffset;
			uint16_t yoffset;
			atlas_image_data(fs::path path, uint16_t width, uint16_t height, uint16_t xoffset, uint16_t yoffset) :
				path{ path }, width{ width }, height{ height }, xoffset{ xoffset }, yoffset{ yoffset }  {};
		};
		std::list<atlas_image_data> image_datas;
	};

	atlas_data AtlasFill(std::list<fs::path> image_paths, parsed_command_line * cmd);
}