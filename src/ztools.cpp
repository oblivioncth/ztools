/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ztools.hpp"

#include <iostream>
#include "Magick++.h"

#include "cmdlinehelper.hpp"
#include "filetypes/tex.hpp"
#include "filetypes/png.hpp"
#include "filetypes/anim.hpp"
#include "filetypes/build.hpp"
#include "filetypes/scml.hpp"
#include "filetypes/atlas.hpp"
#include "binary_io.hpp"
#include "atlasfill.hpp"

void readtex(const fs::path& tex_path, const fs::path& png_path, parsed_command_line * cmd) {
	tex ktex{ tex_path, cmd };
	png image{ ktex, cmd };
	image.write(png_path);
}

void writetex(const fs::path& png_path, const fs::path& tex_path, parsed_command_line * cmd) {
	png image{ png_path, cmd };
	tex ktex{ image, cmd };
	ktex.write(tex_path);
}

void readatlas(const fs::path& xml_path, const fs::path& tex_path, const fs::path& outdir, parsed_command_line * cmd) {
	atlas atlas_image{ xml_path, tex_path, cmd };
	fs::path out_path = outdir;
	if (atlas_image.imagecount() > 1) {
		out_path /= xml_path.stem();
		fs::create_directories(out_path);
	}
	atlas_image.write(out_path);
}

void writeatlas(const fs::path& in_dir, const fs::path& xml_path, const fs::path& tex_path, parsed_command_line * cmd) {
	atlas atlas_image{ in_dir, cmd };
	atlas_image.write(xml_path, tex_path);
}

void writeatlas(const std::list<fs::path>& image_files, const fs::path& xml_path, const fs::path& tex_path, parsed_command_line * cmd) {
	atlas atlas_image{ image_files, cmd };
	atlas_image.write(xml_path, tex_path);
}

void animationprintstrings(const fs::path& bild_path, const fs::path& anim_path, parsed_command_line * cmd) {
	build bild{ bild_path, cmd };
	anim animation{ anim_path, cmd };
	auto symbols = bild.getsymbols();
	symbols.merge(animation.getsymbols());
	symbols.sort();
	symbols.unique();
	for (auto& s : symbols) {
		std::cout << "String: \"" << s << "\"" << std::endl;
	}
}

void writeanimationrenames(const fs::path& in_bild_path, const fs::path& in_anim_path, const fs::path& out_bild_path, const fs::path& out_anim_path, parsed_command_line * cmd) {
	build bild{ in_bild_path, cmd };
	anim animation{ in_anim_path, cmd };
	for (auto& s : cmd->string_renames) {
		std::string oldsymbol = s.substr(0, s.find(":"));
		std::string newsymbol = s.substr(s.find(":") + 1);
		bild.swapsymbol(oldsymbol, newsymbol);
		animation.swapsymbol(oldsymbol, newsymbol);
	}
	bild.write(out_bild_path);
	animation.write(out_anim_path);
}

void readfiles(const fs::path & in_path, const fs::path & outdir, parsed_command_line * cmd) {
	if (fs::is_directory(in_path)) {
		fs::path out_path = outdir / in_path.filename();
		fs::path xml_path = out_path.replace_extension("xml");
		fs::path tex_path = out_path.replace_extension("tex");
		writeatlas(in_path, xml_path, tex_path, cmd);
	} else {
		std::string filetype = get_file_type(in_path);
		if (filetype == "KTEX") {
			fs::path out_path = outdir / in_path.filename();
			out_path.replace_extension("png");
			readtex(in_path, out_path, cmd);
		} else if (filetype == "IMG") {
			if (cmd->no_atlas) {
				fs::path out_path = outdir / in_path.filename();
				out_path.replace_extension("tex");
				writetex(in_path, out_path, cmd);
			} else {
				std::list<fs::path> image_files{ in_path };
				fs::path out_path = outdir / in_path.filename();
				fs::path xml_path = out_path.replace_extension("xml");
				fs::path tex_path = out_path.replace_extension("tex");
				writeatlas(image_files, xml_path, tex_path, cmd);
			}
		} else if (filetype == "XML") {
			fs::path tex_path = in_path;
			tex_path.replace_extension("tex");
			if (fs::is_regular_file(tex_path) && get_file_type(tex_path) == "KTEX") {
				readatlas(in_path, tex_path, outdir, cmd);
			} else {
				throw ZToolsError(strformat("%s was unable to be found, please pass the second texfile manually.", tex_path.string().c_str()));
			}
		} else if (filetype == "SCML") {
			//writeanimation();
		}
	}
}

void readfiles(const fs::path & path, const fs::path & secondarypath, const fs::path & outdir, parsed_command_line * cmd) {
	std::string filetype = get_file_type(path);
	std::string secondaryfiletype = get_file_type(secondarypath);
	if (filetype == "ANIM" || filetype == "BILD") {
		fs::path bild_path{ filetype == "BILD" ? path : secondaryfiletype == "BILD" ? secondarypath : "" };
		fs::path anim_path{ filetype == "ANIM" ? path : secondaryfiletype == "ANIM" ? secondarypath : "" };
		if (anim_path.empty() || bild_path.empty()) {
			throw ZToolsError("Input is not build.bin and anim.bin.");
		}
		if (cmd->print_string) {
			animationprintstrings(bild_path, anim_path, cmd);
		} else if (cmd->string_renames.size() > 0) {
			writeanimationrenames(bild_path, anim_path, outdir / bild_path.filename(), outdir / anim_path.filename(), cmd);
		} else {
			//readanim();
		}
	} else if (filetype == "XML" || filetype == "KTEX") {
		fs::path xml_path{ filetype == "XML" ? path : secondaryfiletype == "XML" ? secondarypath : "" };
		fs::path tex_path{ filetype == "KTEX" ? path : secondaryfiletype == "KTEX" ? secondarypath : "" };
		if (xml_path.empty() || tex_path.empty()) {
			throw ZToolsError("Input is not atlas and tex.");
		}
		readatlas(xml_path, tex_path, outdir, cmd);
	}
}

int main(int argc, char ** argv) {
	fillLayers();
	try {
		Magick::InitializeMagick(*argv);
		parsed_command_line cmdline(argc, argv);
		fs::path input = fs::absolute({cmdline.input_path });
		if (fs::is_directory(input)) {
			if (!cmdline.output_path.empty()) {
				throw ZToolsError("You can't provide two input paths, if you pass a directory as the first input path!");
			}
			fs::path output{ cmdline.secondary_input_path };
			if (output.empty()) {
				output = input.parent_path();
			}
			if (!fs::is_directory(output)) {
				throw ZToolsError("output MUST be a DIRECTORY!");
			}
			//todo implement form of batch processing if the batch processing flag is set
			readfiles(input, output, &cmdline);
		} else {
			fs::path output{ input.parent_path() };
			fs::path secondary;
			bool hassecondaryinput = false;
			if (!cmdline.secondary_input_path.empty() && cmdline.output_path.empty()) {
				secondary = {cmdline.secondary_input_path};
				if (fs::is_directory(secondary)) {
					output = std::move(secondary);
				} else {
					hassecondaryinput = true;
				}
			} else if (!cmdline.secondary_input_path.empty() && !cmdline.output_path.empty()) {
				secondary = {cmdline.secondary_input_path};
				hassecondaryinput = true;
				output = {cmdline.output_path};
			}
			//all that stuff above, just gets us a output, and a secondary input(for stuff like anim.bin + build.bin)
			if (!fs::is_directory(output)) {
				throw ZToolsError("output MUST be a DIRECTORY!");
			}
			if (hassecondaryinput) {
				if (fs::is_directory(secondary)) {
					throw ZToolsError("Secondary input MUST be a FILE!");
				}
				readfiles(input, secondary, output, &cmdline);
			} else {
				readfiles(input, output, &cmdline);
			}
		}
	} catch (std::exception& e) {
		std::cerr << "Error: " << e.what() << std::endl;
		exit(-1);
	}
	return 0;
}