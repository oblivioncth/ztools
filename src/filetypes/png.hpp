/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "../common.hpp"

#include <string>
#include "Magick++/Image.h"

class tex;
class parsed_command_line;

class png {
	friend class tex;
	public:
		png(const fs::path& path, parsed_command_line * cmd);
		png(const uint16_t width, const uint16_t height, const Magick::DrawableList & drawcommands, parsed_command_line * cmd);
		png(const tex& texture, parsed_command_line * cmd);
		void write(const fs::path& path);
		void write(const fs::path& path, const Magick::Geometry& crop);
		Magick::Geometry size() const {
			return image.size();
		};
	private:
		Magick::Image image;
		parsed_command_line * cmd;
};