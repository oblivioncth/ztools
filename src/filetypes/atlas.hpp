/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "../common.hpp"
#include <map>

class png;
class parsed_command_line;

class atlas {
	public:
		atlas(const fs::path& directory, parsed_command_line * cmd);
		atlas(const std::list<fs::path>& pngfiles, parsed_command_line * cmd);
		atlas(const fs::path& xml_path, const fs::path& tex_path, parsed_command_line * cmd);
		~atlas();

		void write(const fs::path& directory);
		void write(const fs::path& xml_path, const fs::path& tex_path);

		int imagecount();
	private:
		parsed_command_line * cmd;

		struct uv_coords {
			uint32_t width;
			uint32_t height;
			uint32_t xoffset;
			uint32_t yoffset;
		};

		std::map<fs::path, uv_coords> imgs;
		std::vector<fs::path> names;

		png * atlas_image;
};