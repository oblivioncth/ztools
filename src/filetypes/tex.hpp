/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "../common.hpp"

#include "Magick++/Blob.h"

class png;
class parsed_command_line;

class tex {
	friend class png;
	public:
		tex(const fs::path&path, parsed_command_line * cmd);
		tex(const png& texture, parsed_command_line * cmd);
		void write(const fs::path& path);
		void write(const fs::path& directory, const int16_t miplevel);

		uint8_t getmipcount() {
			return flags.spec.mipmap_count;
		}

		enum platforms : uint32_t {
			DEFAULT = 0,
			PC = 12,
			PS3 = 10,
			XBOX360 = 11
		};
		enum compression_types : uint32_t {
			DXT1 = 0,
			DXT3 = 1,
			DXT5 = 2,
			RGBA = 4,
			RGB = 5
		};
		enum texture_types : uint32_t {
			_1D = 0,
			_2D = 1,
			_3D = 2,
			CUBEMAP = 3,
		};
	private:
		void getcompressionflags(int compression, bool & iscompressed, int & squishcompression);

		union headerflags {
			uint32_t data;
			struct specs {
				uint32_t platform : 4;
				uint32_t compression : 5;
				uint32_t texture_type : 4;
				uint32_t mipmap_count : 5;
				uint32_t flags : 2;
				uint32_t fill : 12;
			} spec;
			struct pre_caves_specs {
				uint32_t platform : 3;
				uint32_t compression : 3;
				uint32_t texture_type : 3;
				uint32_t mipmap_count : 4;
				uint32_t flags : 1;
				uint32_t fill : 18;
			} pre_caves_spec;
		};

		struct mipmap {
			uint16_t width;
			uint16_t height;
			uint16_t pitch;
			uint32_t datasize;
			Magick::Blob data;
		};

		parsed_command_line * cmd;

		headerflags flags;

		std::string datafmt{"RGBA"};
		std::vector<mipmap> mipmaps;
};