/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "../common.hpp"
#include <map>

class scml;
class parsed_command_line;

class build {
	friend class scml;
	public:
	build(const fs::path& path, parsed_command_line * cmd);
	build(const scml scmlfile, parsed_command_line * cmd);
	void write(const fs::path& path);
	void swapsymbol(const std::string & oldsymbol, const std::string & newsymbol);
	std::list<std::string> getsymbols();
	private:

	std::string getrealstring(uint32_t hash);
	std::string getrealstring(std::string name);

	struct point {
		float x;
		float y;
		float z;
		float u;
		float v;
		float w;
	};
	struct triangle {
		point point1;
		point point2;
		point point3;
	};
	struct frame {
		uint32_t framenumber;
		uint32_t duration;
		float x;
		float y;
		float w;
		float h;
		uint32_t alphaidx;
		uint32_t alphacount;
		std::vector<triangle> triangles;
	};
	struct symbol {
		uint32_t hash;
		uint32_t numframes;
		std::vector<frame> frames;
	};

	parsed_command_line * cmd;

	uint32_t version;
	uint32_t numsymbols;
	uint32_t numframes;
	std::string buildname;

	uint32_t numatlases;

	std::vector<std::string> atlaslist;

	std::vector<symbol> symbols;
	uint32_t numalphaverts;

	uint32_t hashcount;

	std::vector<uint32_t> hashlist;

	//hash, string
	std::map<uint32_t, std::string> symbollist;
};